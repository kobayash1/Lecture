* Chapter 5 - Cognitive Development in Infancy and Toddlerhood
** Introduction
*** Cognitive Development in the First Two Years
    - throughout development, cognition and language mutually support each other
    - newborns become assertive, purposeful beings who solve simple problems and start to master language
    - toddlers' first words build on early cognitive achievements, and new words and expressions increase the speed and flexibility of their thinking
** Piaget's Cognitive-Development Theory
*** Piaget's Schemes
    - psychological structures
    - organized ways of making sense of experience
    - change with age
      - first schemes are sensorimotor action patterns
      - later schemes are deliberate and creative, evidence of thinking before acting
    - change through adaptation and organization
*** Building Schemes
**** Adaptation
     - building schemes through direct interaction with the environment
**** Assimilation
     - using current schemes to interpret the world
**** Accomodation
     - creating new schemes and adjusting old ones to better fit the envirionment
*** Using Assimilation and Accommodation
**** Cognitive Equilibrium
     - what we know matches with what we see
       - leads to assimilation
**** Cognitive Disequilibrium
     - what we know does not match with what we see
       - leads to accomodation
*** Changing Schemes Through Organization
    - schemes reach equilibrium when they become part of a broad network of structures that can be jointly applied to surrounding world
**** Organization
     - internal process
     - linking together of schemes into an interconnected cognitive system
*** Sensorimotor Stage
    - birth to age 2
    - building schems through sensory and motor exploration
    - intentional behavior combines schemes into more complex actions
**** Circular Reaction
     - stumble onto a new experience
     - repetition of chance behaviors forms them into schemes
*** Sensorimotor Substages
**** Reflexive Schemes
     - 0-1 month
     - newborn reflexes
**** Primary Circular Reactions
     - 1-4 months
     - simple motor habis centered around own body
**** Secondary Circular Reactions
     - 4-8 months
     - repetition of interesting effects, imitation of familiar behaviors
**** Coordination of Secondary Circular Reactions
     - 8-12 months
     - intentional, goal-directed behavior
     - beginning of object permanence
**** Tertiary Circular Reactions
     - 12-18 months
     - exploration of object properties through novel actions
**** Mental Representation
     - 18 months - 2 years
     - internal depictions of objects and events
     - advanced object permanence
*** Object Permanence
    - understanding that objects continue to exist when out of sight
    - present within first few months of life
    - mastery is gradual, more complex with age
      - awareness not yet complete
        - A-not-B search error
      - full understanding revealed by problems ninvolving invisible displacement
*** Mental Representation
    - internal depictions the mind can manipulate
      - images
        - mental pictures of objects, people, spaces
      - concepts
        - categories of similar objects or events
    - permits
      - advanced object permanence
      - deferred/inferred imitation
      - make-believe play
      - problem solving
      - symbolic understanding
*** Deferred and Inferred Imitation
**** Deferred Imitation
     - ability to remember and copy past behavior of a model who is no longer present
     - enriches toddlers' range of sensorimotor schemes
**** Inferred Imitation
     - requires infferring others' intentions
     - more likely to imitate purposeful rather than accidental behaviors
*** Problem Solving
    - infants employ:
      - intentional means-end action sequences (7-8 months)
      - tools to manipulate objects (7-8 months)
      - analogy (10-12 months)
    - by end of first year, have some ability to:
      - move beyond trial-and-error experimentation
      - represent a solution mentally and use it in new contexts
*** Symbolic Understanding
**** Displaced Reference
     - realization that words can cue mental images of things not present
     - emerges around first birthday
     - expands as memory and vocabulary improve
     - facilitates learning and communication
*** Evaluation of Sensorimotor Stage
**** Capacities that Develop When Piaget Suggested
     - anticipation of events
     - hidden object search
     - A-not-B object search
     - Varying sensorimotor schemes
     - make-believe play
     - symbolic understanding of pictures
**** Capacities that Develop Earlier than Piaget Suggested
     - secondary circular reaction
     - understanding of object properties
     - first signs of object permanence
     - deferred imitation
     - problem solving by analogy
     - displaced reference of words
*** Core Knowledge Perspective
    - infants are born with innate knowledge systems, or core domains of thought
      - permit a quick grasp of new information
      - support rapid early development
    - experience is essential to textend core knowledge
    - suggested domains of core knowledge:
      - physical
      - linguistic
      - psychological
      - numerical
*** Broad Disagreement with Piaget
    - many cognitive changes of infancy are gradual and continuous rather than abrupt and stagelike
    - various aspects of infant cognition change unevenly due to varying experiences with different tasks
    - findings serve as basis for information processing
** Information Processing
*** Information Processing
    - information is held for processing in three areas
**** Sensory Register
     - briefly stores sights and sounds
**** Short-Term Memory Store
     - attended-to information is retained briefly and "worked" on
     - working memory
       - number of items that can be briefly held in mind while also monitoring or manipulating them
**** Long-Term Memory
     - permanent knowledge base
*** Managing the Cognitive System's Activities
**** Central Executive
     - directs flow of information
     - conscious part of mind
     - coordinates incoming information with existing information
     - selects, applies, and monitors strategies that aid:
       - memory storage, comprehension, reasoning, and problem solving
**** Automatic Processes
     - well-learned, require no space in working memory
     - can be done while focusing on other information
*** Cognitive System Improvements During Childhood and Adolescence
    - greater working-memory capacity
    - increased speed for working on information
    - gains in executive function
**** Executive Function
     - mental operations and strategies for cognitively challenging situations
       - controlling attention
       - coordinating informatioin in working memory
       - planning
*** Cognitive Gains in Infancy and Toddlerhood
**** Attention
     - improved efficiency, ability to shift focus
     - sustained attention
**** Memory
     - longer retention intervals
     - recognition and recall improve steadily with age
     - long-term recall advances as brain's neural circuits develop
*** Categorization
    - grouping similar objects and events reduces large amount of new information encountered
    - infants
      - young infants sort based on physical properties
      - older infants expand in variety of features
    - toddlers
      - categorize flexibly, considering multiple groupings
      - shift from perceptual to conceptual categorization
** Infantile Amnesia
   - inability to recall events that happened prior to age 2 or 3
   - possible causes
     - hippocampus not fully developed during that age
     - nonverbal nature of early memory processing
     - lack of clear self-image
** Social Context of Early Cognitive Development
*** Vygotsky's Sociocultural Theory
    - social and cultural contexts affect how a child's cognitive world is structured
    - complex mental activities develop through joint activities with more mature individuals of their society
**** Zone of Proximal Development
     - tasks too difficult for child to do alone but possible with help of a skilled partner
**** Scaffolding
     - promotes learning at all ages
*** Summary of Cognitive Development Influeneces
**** Piaget
     - infants and toddlers create new schemes by acting on the physical world
**** Information Processing
     - certain skills become better developed as children represent their experiences more efficiently and meaningfully
**** Vygotsky
     - many aspects of cognitive development are socially mediated
** Cultural Influences
*** Social Origins of Make-Believe Play
    - greatly extends toddlers' cognitive and social skills
    - taught and scaffolded under guidance of experts
      - adults or older siblings; cultures vary
      - toddlers must be encouraged to participate
      - rich cues help distinguish pretend from real acts
    - when adults participate, toddlers' make-believe play is more complex and teaches cultural values
** Individual Differences in Early Mental Development
*** Infant and Toddler Intelligence Tests
    - measure individual variations in development
    - largely used for screening
**** Bayley Scales of Infant and Toddler Development
     - current edition:  Bayley-III
     - subtests
       - coginitive
       - language
       - motor scales
     - parental report
       - social-emotional and adaptive behavior scales
*** Computing Intelligence Test Scores
    - infant tests do now assess same aspects of intelligence
      - often test perceptual and motor responses
      - largely used to screen for developmental problems
**** Intelligence Quotient
     - comparison with typical performance for age
     - standardization
       - based on results from laarge sample
     - normal distribution
       - bell curve
*** Home Life Assessment
    - linked to toddlers' mental test performance
**** Home Observation for Measurement of the Environment (HOME)
     - organization and safety of physical environment
     - provision of appropriate play materials
     - parental emotional and verbal responsiveness
     - parental acceptance of child
     - parental involvement with child
     - opportunities for variety in daily stimulation
*** Developmentally Appropriate Infant and Toddler Child Care
    - characteristics include:
      - physical setting
      - toys and equipment
      - caregiver-child ratio
      - daily activities
      - interactions among adults and children
      - caregiver qualifications
      - relationships with parents
      - licensing and accreditation
*** Early Intervention for At-Risk Infants and Toddlers
    - persistent poverty causes declines in test scores
      - stressful home environments increase likelihood children will remain poor as adults
**** Interventions to Break Cycle of Poverty
     - center-based child care and social services
     - home-based training for parents by skilled adult
     - participating children score higher on mental tests and maintain benefits
     - Early Head Start
       - federal program with 1,000 sites
** Language Development
*** Language Development Milestones
    | Second half of year 1 | Distinguishes language sounds, segments speech into word and phrase units |
    | 12 months             | Says first word                                                           |
    | 1.5-2 years           | Combines two words                                                        |
    | 3.5 years             | Forms more complex sentences                                              |
    | 6 years               | Understands meaning of about 14,000 words                                 |
*** Theories of Language Development
**** Nativist (Chomsky)
     - Language Acquisition Device
       - innate system containing universal grammar
     - infants biologically prepared to learn language
**** Interactionist
     - interactions between inner capacities and environmental influences
     - information-processing view
     - social-interactionist view
*** Getting Ready to Talk
    - first speech sounds
      - cooing (2 months)
      - babbling (6 months)
    - becoming a communicator
      - joint attention of child and caregiver (3-4 months)
      - give-and-take: mutual imitation of sounds (3 months)
      - preverbal gestures (end of first year)
*** Starting to Talk
    - around age 1, acquires one to three new words per week, which gradually accelerates
    - first words
      - underextension
        - applying newly learned word too narrowly
      - overextension
        - applying word too broadly
    - two-word utterances
      - telegraphic speech
      - high-content word pairings
      - copying adult gradually shifts to using grammatical rules
*** Individual Differences
    - first word
      - from 8 to 18 months
    - influential factors
      - gender; girls slightly ahead of boys
      - temperament
      - caregiver-child conversation, reading
      - vocabulary growth
*** Infant-Directed Speech (IDS)
    - consists of
      - short sentences
      - high-pitched, exaggerated expression
      - clear pronunciation
      - distinct pauses between speech segments
      - clear gestures to support verbal meaning
      - repetition of new words in many contexts
    - IDS and parent-child conversaation create a zone of proximal development for language acquisistion
