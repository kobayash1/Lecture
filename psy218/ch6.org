* Chapter 6 - Emotional and Social Development in Infancy and Toddlerhood
** Erikson's Theory of Infant and Toddler Personality
*** Psychosocial Stages During Infancy and Toddlerhood
    | Erikson's Stage             | Needed from Caregivers                      |
    | Basic trust vs mistrust     | responsiveness                              |
    | (first year)                | sympathetic, loving balance of care         |
    | Autonomy vs shame and doubt | suitable guidance and reasonable choices    |
    | (second year)               | reasonable expectations for impulse control |
*** Basic Emotions
    - earliest emotions consist of two arousal states:
      - attraction to pleasant stimulation
      - withdrawal from unpleasant stimulation
    - emotions gradually become organized and specific, supported by sensitive caregiving and mirroring
    - face, voice, and posture from patterns with age
*** First Appearance of Basic Emotions
**** Happiness
     - smile
       - from birth
     - social smile
       - 6-10 weeks
     - laughter
       - 3-4 months
**** Anger and Sadness
     - general distress
       - from birth
     - angry expressions
       - 4-6 months
     - sadness
       - response to disrupted caregiver-infant communication
**** Fear
     - first fears
       - 6 months
       - keeps exploration in check
       - caregiver as secure base
     - stranger anxiety
       - most frequent expression of fear
*** Responding to Emotions of Others
    - matching caregiver's tone (initial months)
    - sensitivity to face-to-face interactions (3 months)
    - responding to emotional expressions as organized wholes (5 months)
    - social referencing (8-10 months)
*** Social Referencing
    - seeking emotional information from a trusted person to appraise an uncertain situation
    - helps toddlers:
      - evaluate surroundings
      - guide their actions
      - understand others
*** Self-Conscious Emotions
    - guilt, shame, embarrassment, envy, and pride
    - appear middle of second year
    - injure or enhance sense of self
    - require self-awareness and adult instruction in wwhen to feel emotions
*** Emotional Self-Regulation
    - emerges as motor and language skills develp
    - improves rapidly during first few years due to brain development and caregiver support
    - caregivers teach socially approved ways of expression
    - end of year 2, vocabulary forms for talking about emotions, but unable to manage them
      - tantrums occur
** Temperament and Development
*** Temperament
    - early-appearing, stable individual differences in reactivity and self-regulation
**** Thomas and Chess's Model
     - easy children (40%)
     - difficult children (10%)
     - slow-to-warm-up children (15%)
     - unclassified children (35%)
*** Rothbart's Model of Temperament
**** Reactivity
     - activity level
     - attention span/persistence
     - fearful distress
     - irritable distress
     - positive affect
**** Self-Regulation
     - effortful-control
       - predicts favorable adjustment
*** Stability of Temperament
    - low in infancy and toddlerhood
    - more stable after age 3
    - influential factors:
      - brain development enabling impulse suppression
      - effortful control and emotional reactivity
      - child rearing
*** Heredity and Environment in Temperament
**** Genetic Influences
     - responsible for half of individual differences
     - vary with trait and age
     - gender and ethnic differences
     - short 5-HTTLPR or DRD4 7-repeat genotypes
**** Environmental Influences
     - nutrition
     - quality of caregiving
     - cultural variations
     - gender stereotyping
     - parental distinctions among siblings
*** Genes Affecting Temperament
    - children highly susceptible to parenting quality
      - benefit most from interventions to improve child rearing
**** Short 5-HTTLPR
     - base pair repetition on chromosome 7
     - interferes with inhibitory neurotransmitter serotonin
     - increased risk of self-regulation difficulties
**** DRDR 7-repeat
     - base pair repetition on chromosome 11
     - linked to impulsive overactive behavior
*** Goodness-of-Fit Model
    - encourages an effective match between child rearing and child's temperament
    - children have unique dispositions that adults must accept
    - best accomplished early
    - successful child rearing:
      - responsive to child's temperament
      - simultaneously encourages more adaptive functioning
** Development of Attachment
*** Bowlby's Ethological Theory of Attachment
    - begins with innate signals that keep parent nearby
      - over time, affectionate bond forms
      - preattachment
      - attachment-in-the-making
      - clear-cut attachment
        - separation anxiety
      - formation of reciprocal relationship
**** Internal Working Model
     - expectations about availability of attachment figures
*** Episodes in Strange Situation
    1. Parent and baby introduced to playroom
    2. Parent sits while baby plays with toys
    3. Stranger enters, is seated, talks to parent
    4. Parent leaves; stranger offers comfort if baby is upset
    5. Parent returns, greets baby, offers comfort if necessary; stranger leaves
    6. Parent leaves
    7. Stranger enters, offers comfort
    8. Parent returns, greets baby, offers comfort if necessary, tries to reinterest baby in toys
*** Mearuing Security of Attachment
**** Strange Situation (age 1-2)
     - secure: 60%
     - insecure-avoidant: 15%
     - insecure-resistant: 10%
     - disorganized/disoriented: 15%
**** Attachment Q-Sort (age 1-5)
     - home observation of 90 behaviors
     - more precise measure than Strange Situation
*** Factors that Affect Attachment Security
    - early availability of consistent caregiver
    - quality of caregiving
    - infant characteristics
    - family circumstances
    - parents' internal working models
      - view of their own childhood attachment experiences
*** Infant Characteristics that Pose Risks to Attachment Security
    - may result in attachment insecurity:
      - infant emotional reactivity
      - certain genotypes combined with insensitive parenting
      - mother's experience of trauma
*** Family Circumstances
    - factors fostering attachment security:
      - reducing stress
      - improving parent-child communication
      - social support
**** Indirect Stressors
     - can cause insensitive caregiving
     - e.g. financial, marital
**** Direct Stressors
     - disrupt emotional climate and routines
     - e.g. angry adult interactions
*** Multiple Attachments
    - fathers
    - siblings
    - grandparents
    - professional caregivers
*** Siblings' Sensitive Caregiving
    - new baby can be stressful for older siblings
    - children treat older siblings as attachment figures
    - siblings generally develop rich emotional relationship
    - certain temperamental traits increase likelihood of sibling conflict
      - e.g. high emotional reactivity
    - maternal warmth toward both children supports positive sibling interaction
*** Attachment and Later Development
    - long-term effects of early attachment security are conditional
      - depend on the quality of future relationships
    - continuity of caregiving promotes favorable development
** Self-Development
*** Milestones in Developing Self-Awareness
**** Beginnings of Self-Awareness
     - newborn capacity for intermodal perception
     - discriminate own limg and facial movements
**** Self-Recognition
     - point to self in photos
     - refer to self by name or by prounoun "I" or "me"
     - recognize own shadow
**** Empathy
     - communicate concern when others are distressed
     - offer others what they consider comforting
*** Experiences Contributing to Self-Awareness
    - infants act on environment, distinguishing self, other people, and objects
    - joint attention lets toddlers compare their own and others' reactions to objects and events
    - cultural variations exist:
      - autonomous child rearing facilitates earlier mirror self-recognition
      - relational child rearing facilitates earlier capacity for compliance
*** Categorical Self
    - classifying self and others based on:
      - age
      - physical characteristics
      - goodness vs badness
    - develops between ages 1.5 and 2.5
    - used to organize their own behavior, including gender-typed activity
*** Self-Control
    - depends on
      - awareness of self as separate, autonomous being
      - confidence in directing own actions
      - memory for caregiver's directives
**** Compliance
     - relects clear awareness of caregivers' wishes and expectations
**** Delay of Gratification
     - influenced by quality of caregiving
*** Fostering Toddlers' Compliance and Self-Control
    - respond with sensitivity and encouragement
    - give advance notice of change in activity
    - offer many prompts and reminders
    - reinforce self-controlled behavior
    - encourage sustained attention
    - support language development
    - increase rules gradually
