* Chapter 4 - Physical Development in Infancy and Toddlerhood
** The First Two Years
   - infancy (first year) through toddlerhood (second year)
   - rapid changes in body and brain spport learning, motor skills, and perceptual capacities
   - motor, perceptual, cognitive, and social development mutually influence one another
     - e.g. when child walks, hands are free to explore and interact with physical world
** Body Growth
   - faster growth than at any other time
     | Age           | Height                        | Weight             |
     |---------------+-------------------------------+--------------------|
     | Newborn       | 20" long                      | 7.5 lbs            |
     | End of year 1 | 32"; 50% greater than at birth | 22 lbs; triple     |
     | End of year 2 | 36"; 75% greater than at birth | 30 lbs; quadruples |
*** Changes in Body Proportions
    - cephalocaudal trend
      - "head to tail"
      - head develops more rapidly than lower part of body
    - proximodistal trend
      - "near to far"
      - growth proceeds from body outward
*** Individual and Group Differences
    - growth norms
      - height and weight averages
    - gender and ethnic differences are apparent
    - individual variation due to many factors, such as nutrition
    - skeletal age is best estimate of physical maturity
** Brain Development
*** Neurons and Their Connective Fibers
    | Neurons           | Nerve cells that store and transmit information                 |
    | Synapses          | Tiny gaps between neurons                                       |
    | Neurotransmitters | Chemicals released by neurons that send messages across synapse |
*** Development of Neurons
    - programmed cell death
      - neurons die to make space for new connective structures
    - stimulation is vital ofr survival of neurons and formation of new synapses
    - synaptic pruning
      - returns seldom-stimulated neurons to an uncommitted state
*** Myelination
    - coating of neural fibers with myelin, an insulating fatty sheath
    - improves efficiency of message transfer
    - glial cells
      - responsible for myelination
      - account for half of the brain's volume
      - multiply rapidly in first two years
*** Measures of Brain Functioning
    - brain-wave patterns and changes in electrical activity
      - electroencephalogram (EEG)
      - event-related potentials (ERP's)
    - brain regions and functioning
      - 3-D neuroimaging
        - functional magnetic resonance imaging (fMRI)
        - positron emission tomography (PET)
        - near-infrared spectroscopy (NIRS)
*** Cerebral Cortex and Cortical Regions
    - cerebral cortex
      - 85% of brain's weight, surrounds rest of brain
    - cortical regions develop as capcities emerge
      - first year
        - auditory and visual
        - body movement areas
      - infancy through preschool
        - language areas
    - prefrontal cortex
      - responsible for complex thought
      - functions more effectively from age 2 months on
*** Lateralization of the Cerebral Cortex
**** Left Hemisphere
     - sequential, analytic processing
     - verbal communication
     - positive emotion
**** Right Hemisphere
     - holistic, integrative processing
     - making sense of spatial iinformation
     - regulating negative emotion
*** Brain Plasticity
    - at birth, hemispheres have begun to specialize
    - highly plastic cerebral cortex during first few years:
      - many areas not yet committed to specific functions
      - high capacity for learning
      - early experiences influence its organization
    - if part of cortex is damaged, other areas can take over
*** Sensitive Periods in Brain Development
    - appropriate stimulation is vital for brain growth
      - counteracts negative effects of depleted environment
    - early, extrem sensory deprivation results in permanent brain damage and loss of functions
    - rushing early learning overwhelms neural circuits, impedes brain's potential
**** Experience-expectant Brain Growth
     - occurs early and naturally
     - rapidly developing organization
     - depends on ordinary experiences "expected" by brain for normal growth
**** Experience-dependent Brain Growth
     - occurs throughout our lives
     - growth and refinement
     - results from specific, individual learning experiences
*** Changing States of Arousal
    - sleep-wake pattern gradually shifts to night-day schedules, and total sleep time declines
    - changes influenced by:
      - brain development
      - cultural beliefs and practices
      - parents' needs and schedule
      - increased melatonin secretion
** Cultural Influences
*** Cultural Variation in Infant Sleeping Arrangements
    - parent-infant cosleeping is the norm for 90% of world's population
    - cultural values of interdependence vs independence influence sleeping arrangements
    - possible benefits:
      - helps infants sleep
      - breastfeeding more convenient
      - cosleeping safely may protect babies at risk for SIDS
** Influences on Early Physical Growth
*** Influences on Early Growth
    - heredity
    - nutrition
      - breastfeeding vs bottle-feeding
      - risks of overfeeding
    - malnutrition
*** Heredity
    - large influence on rate of physical growth when diet and health are adequate
    - also affects height and weight
    - catch-up growth
      - return to genetically influenced growth path once negative conditions corrected
*** Nutrition and Breastfeeding
     - crucial for development in first two years
     - mothers in developing world often unaware of benefits
       - use low-grade commercial formula and ingredients
**** Benefits of Breastfeeding
     - ensures nutritional completeness
     - provides correct fat-protein balance
     - helps ensure healthy physical growth
     - protects against disease
     - protects against faulty jaw and tooth development
     - ensures digestibility
     - smooths transition to solid foods
*** Preventing Overweight Children
    - breasfeed exclusively for the first six months
    - avoid giving foods rich in sugar, salt, and saturated fats
    - provide opportunities for energetic play
*** Malnutrition
    - affects 2.1 million children annually
      - stunts growth of one-third of all children under 5
    - dietary diseases of severly malnourished children
      - marasmus
        - during first year, due to inadeuate feeding
      - kwashiorkor
        - after weaning, due to low protein diet
      - causes long-term damage to brain and organs, affects learning and behavior, and disrupts development
** Learning Capacities
*** Classical Conditioning
    - pairs neutral stimulus with one that prompts reflexive response
    - helps infants recognize which events usually occur together
    - envrionment becomes more orderly and predictable
*** Operant Conditioning
    - infant acts, or operates, on the environment
    - reinforcer: increases occurence of response
      - presenting desirable stimulus
      - removing unpleasant stimulus
    - punishment: decreases occurence of response
      - presenting unpleasant stimulus
      - removing desirable stimulus
*** Habituation and Recovery
    - habituation
      - gradual reduction in the strength of a response due to repetitive stimulation
      - indicators of loss of interest:
        - decline in time spent looking at stimulus
        - decline in heart rate, respiration rate, and brain activity
      - recovery
        - a new stimulus causes responsiveness to return to a high level
      - used to study infant perception and cognition
*** Imitation
    - infants are born with primitive ability to imitate
    - mirror neurons may provide biological basis
    - ability to imitate expands greatly over first two years
    - powerful means of learning and exploring social world
** Motor Development
   - new achievements build on previous ones
   - gross-motor development
     - crawling
     - standing
     - walking
   - fine-motor development
     - reaching
     - grasping
   - rate of motor progress varies widely
*** Gross- and Fine-Motor Development in the First Two Years
    | Motor Skill          | Average Age Achieved |
    |----------------------+----------------------|
    | Grasps cube          | 3 months, 3 weeks    |
    | Sits up alone        | 7 months             |
    | Crawls               | 7 months             |
    | Pulls to stand       | 8 months             |
    | Plays pat-a-cake     | 9 months, 3 weeks    |
    | Walks alone          | 11 months, 3 weeks   |
    | Scribbles vigorously | 14 months            |
    | Jumps in place       | 23 months, 2 weeks   |
*** Motor Skills as Dynamic Systems
    - acquisition of increasingly complex systems of action
      - e.g. crawling, standing, and stepping unite into walking
    - each new skill is joint product of:
      - CNS development
      - body's movement capacities
      - goals child has in mind
      - environmental supports for the skill
*** Cultural Variations in Motor Development
    - cultural values and infant-rearing customs affect motor development
    - sandbag-reared babies in rural China are delayed in sitting and walking
    - Kipsigis of Kenya use formal exercises that promote early motor maturity
*** Milestones of Reaching and Grasping
    - newborn
      - prereaching
    - 3-4 months
      - ulnar grasp
    - 4-5 months
      - transferring object from hand to hand
    - 9 months
      - pincer grasp
** Perceptual Development
*** Developments in Hearing
    | 4-7 months  | Sense of musical phrasing                                            |
    | 6-7 months  | Distinguishes musical tunes based on variations in rhythmic patterns |
    | 6-8 months  | "Screens out" sounds not used in native languages                    |
    | 6-12 months | Detects regularities in human speech                                 |
    | 7-9 months  | Begins to divide speech stream into word-like units                  |
*** Statistical Learning Capacity
    - by analyzing speech stream for patterns, babies:
      - acquire speech structures for which they will later learn meanings
      - extract patterns from complex, continuous speech
    - present in first weeks of life
    - extends to visual stimuli
*** Visual Development
    - supported by rapid maturation of eyes and visual centers in brain
**** Milestones
***** 2 Months
      - focus
***** 4 Months
      - color vision
***** 6 Months
      - acuity
      - scanning
      - tracking
***** 6-7 Months
      - depth perception
*** Depth Perception
**** Milestones
***** 3-4 Weeks
      - sensitivity to motion
***** 2-3 Months
      - sensitivity to binocular depth
***** 5-7 Months
      - sensitivity to pictoral depth
**** Independent Movement
     - promotes three-dimensional understanding
     - helps infants remember object locations and find hidden objects
*** Milestones in Pattern Perception
**** 2 Months
     - detection of detail
       - sensitive to contrast in complex patterns
     - prefers patterns with more contrast
**** 2-3 Months
     - improved scanning ability
       - explores pattern features, pausing briefly to look at each part
**** 4 Months
     - detects pattern organizaton
       - perceives subjective boundaries that are not really present
**** 12 Months
     - detects familiar objects represented by incomplete drawings
*** Milestones in Face Perception
**** Newborn
     - prefers implified drawings of faces with naturally arraged features, with eyes open and a direct gaze
**** 2 Months
     - prefers complex facial patterns to other complex stimulus arrangements, and mother's detailed facial features to another woman's
**** 3 Months
     - makes fine distinctions among the features of different, moderately similar faces
**** 5 Months
     - prerceives emotional expressions as meaningful wholes
**** 7 Months
     - discriminates among a wider range of facial expressions
       - e.g. happiness, surprise, anger
*** Face Perception
    - early experience promotes perceptual narrowing with respect to gender and race:
      - as early as 3 months, prefers and more easily discriminated female faces than male
      - if exposed mostly to members of own race, by 3-6 months, shows own-race bias
        - between 6-9 months, has more difficulty discriminating other-race faces
*** Intermodal Perception
    - capacity to perceive streams of simultaneous, multisensory input as integrated whole
    - rapid development during first 6 months supports:
      - perceptual understanding of physical world
      - social and language processing
**** Amodal Sensory Properties
     - information that overlaps multiple sensory systems
     - e.g.: the sight and sound of a bouncing ball
*** Milestones in Intermodal Perception
**** Newborn
     - perceives amodal sensory properties
**** 3-5 Months
     - matches faces with voices on basis of lip-voice synchrony, emotional expressiong, and speaker's age and gender
**** 6 Months
     - perceives and remembers unique face-voice pairings of unfamiliar adults
*** Differentiation Theory
    - infants actively seek features that remain stable among an ever-changing environment:
      - e.g.: analyzing speech stream for regularities
      - over time, detect finer and finer invariant features
    - applies to
      - intermodal perception
      - pattern perception
