* Chapter 1 - History, Theory, & Research Strategies
** Developmental Science
   - the study of constancy and change throughout the lifespan
   - field is
     - scientific
     - applied
     - interdisciplinary
** Theory
   - an orderly, integrated set of statements that
     - describes behavior
     - explains behavior
     - predicts behavior
** Basic Issues in Development
*** Continuous or Discontinuous?
**** Continuous
     - skills acquired at birth
**** Discontinuous
     - skills acquired and developed through life
*** One Course of Development or Many?
*** Contexts of Development
    - unique combinations of personal and environmental circumstances that can alter path of change
*** Relative Influence of Nature and Nurture?
**** Nature
     - hereditary information
     - received from parents at conception
**** Nurture
     - physical and social forces
     - influences biological makeup and psychological experiences
*** Stability and Plasticity
**** Stability
    - persistence of individual differences
    - lifelong patterns established by early experiences
**** Plasticity
    - development is open to lifelong change
    - change occurs based on influential experiences
** Lifespan Perspective: A Balanced Point of View
*** Lifespan Perspective
    - development is
      - lifelong
      - multidimensional and multidirectional
      - highly plastic
      - influenced by multiple, interacting forces
*** Influences on Development
    - multiple, interacting forces that are:
      - age-graded
      - history-graded
      - nonnormative
*** Periods of Development
    | Prenatal                | Conception to birth |
    | Infancy and toddlerhood |    Birth to 2 years |
    | Early childhood         |                 2-6 |
    | Middle childhood        |                6-11 |
    | Adolescence             |               11-18 |
    | Early adulthood         |               18-40 |
    | Middle adulthood        |               40-65 |
    | Late adulthood          |                 65+ |
*** Domains of Development
    | Domain               | Changes In                  |
    |----------------------+-----------------------------|
    | Physical             | body                        |
    | Cognitive            | intellectual abilities      |
    | Emotional and Social | interpersonal relationships |
** Biology and Environment: Resilience
   - ability to adapt effectively in the face of threats to development
*** Factors in Resilience:
    - personal characteristics
    - warm parental relationship
    - social support outside family
    - community resources and opportunities
** Early Scientific Theories
   | Evolutionary theory     | Darwin                                                                                                               |
   | Normative approach      | Hall and Gesell: Development as a maturational process                                                             |
   | Mental testing movement | Binet and Simon: Early developers of intelligence testing; sparked interest in individual differences in development |
** Mid-twentieth Centry Theories
*** Psychoanalytic Perspective
**** Freud and Erikson
     - emphasis on individual's unique life history
     - conflicts between biological dives and social expectations
     - id, ego, superego blah blah blah
     - Erikson's Psychosocial Stages
        | Basic trust vs mistrust     | Birth - 1 year |
        | Autonomy vs shame and doubt |            1-3 |
        | Initiative vs guilt         |            3-6 |
        | Industry vs inferiority     |           6-11 |
        | Identity vs role confusion  |          12-18 |
        | Intimacy vs isolation       |          18-40 |
        | Generativity vs stagnation  |          40-65 |
        | Ego integrity vs despair    |            65+ |
*** Behaviorism
    | Classical conditioning | Stimulus -- response       |
    | Operant conditioning   | Reinforcers and punishment |
*** Social Learning Theory
    | Modeling or obersvational learning | A baby claps her hands after her mother does |
    | Cognition                          | Emphasized today; social-cognitive approach  |
    | Personal standards                 | Children develop a sense of self-efficacy    |
*** Behaviorism and Social Learning Theory Pros & Cons
**** Contributions
     - behavior modification
     - modeling, observational learning
**** Limitations
     - narrow view of envrionmental influences
     - underestimation of individual's active role
*** Piaget's Stages of Cognitive Development
    | Stages               | Period of Development | Description                                              |
    | Sensorimotor         |       Birth - 2 years | Use senses to explore and learn world                    |
    | Preoperational       |                   2-7 | Use symbols, learn language, make-believe play, no logic |
    | Concrete operational |                  7-11 | Logic and reason, unable to think in abstract            |
    | Formal operational   |                   11+ | Abstract thinking, hypotheses, logic and reason          |
** Recent Theoretical Perspectives
*** Information Processing
    - human mind as a symbol-manipulating system
    - researchers often design flowcharts to map problem-solving steps
    - development as a continuous process
    - strength: provides precise accounts of how children and adults tackle many cognitive tasks
    - limitation: lacks insight into nonlinear cognition, such as imagination and creativity
*** Developmental Neuroscience
**** Developmental Cognitive Neuroscience
     - relationship between brain activity and cognitive processing and behavior patterns
     - incorporates psychology, biology, neuroscience, medicine
**** Developmental Social Neuroscience
     - relationship between brain activity and emotional and social development
     - interest in adolescents' risk-taking behavior, impact of extreme adversity, neurological bases of autism
*** Ethology
    - concerned with the adaptive, or survival, value of behavior and its evolutionary history
    - roots traced to work of Darwin
      - imprinting
      - critical period
      - sensitive period
**** Critical period
     - limited time span during which the individual is biologically prepared to acquire certain adaptive behaviors
     - needs support of a stimulating environment
**** Sensitive period
     - an optimal time for certain capacities to emerge
     - individual is especially responsive to environmental influences
     - boundaries less well-defined than those of a critical period
*** Evolutionary Developmental Psychology
    - seeks to understand adaptive value of species-wide competencies
    - studies cognitive, emotional, and social competencies as they change with age
    - has begun to address the adaptiveness of human longevity
*** Vygotsky's Sociocultural Theory
    - focuses on how culture (values, beliefs, customs, skills) is transmitted to next generation
    - social interaction is necessary for children to acquire culture
      - especially cooperative dialogues with more knowledgeable members of society
*** Ecological Systems Theory
    - person develops within complex system of relationships affected by multiple levels of surrounding environment
    - chronosystem
      - dynamic, ever-changing nature of person's environment
    - person and environment form a network of interdependent effects
**** Layers of Environment
     - microsystem
     - mesosystem
     - exosystem
     - macrosystem
** Stances of Major Theories on Basic Issues in Human Development
   | Theory                            | Continuity    | Courses | Nature/Nurture   |
   |-----------------------------------+---------------+---------+------------------|
   | Psychoanalytic                    | Discontinuous | One     | Both             |
   | Behaviorism & Social Learning     | Continuous    | Many    | Empasize Nurture |
   | Piaget's Coginitive-Developmental | Discontinuous | One     | Both             |
   | Information Processing            | Continuous    | One     | Both             |
   | Ethology                          | Both          | One     | Both             |
   | Evolutionary Developmental        | Both          | One     | Both             |
   | Vygotsky's                        | Both          | Many    | Both             |
   | Ecological Systems                | Not Specified | Many    | Both             |
   | Lifespan Perspective              | Both          | Many    | Both             |
** Studying Development
*** Scientific Research
    - hypothesis
      - prediction drawn from a theory
    - research methods
      - activities of participants
    - research designs
      - overal plans for research studies
*** Systematic Observation
**** Naturalistic Observation
     - behavior recorded in the field or natural environment
     - not all individuals have same opportunity to display behavior
     - cannot control conditions
**** Structured observation
     - lab situation set up to evoke behavior
     - all participants have equal chance to display behavior
     - may ot be typical of participants' everyday behaviors
*** Self-Reports
**** Clinical Interview
     - flexible conversation to get participants' points of view
     - reflects everyday life and provides plarge amount of informationin short amount of time
     - may not result in accurate reporting of information
**** Structured Interview
     - each participant is asked same questins in the same way
     - not as in-depth as clinical interview
*** Case Study
    - brings together wide range of information, including interviews, observations, test scores
    - well-suited to studying individuals who are few in number but vary widely in characterisitcs
    - may be influenced by researcher biases, and conclusions may not generalize to anyone other than person studied
*** Ethonography
    - study of group of people
    - participant observation of a culture or distinct social group
    - mix of observations, self-reports, interpretation by investigator
    - investigators may observe selectively or misinterpret what they see
    - findings cannot be assumed to generalize beyond people and settings of research study
*** Correlational Research Design
    - researchers gather information on individuals, without altering experiences
    - studies relationships between participants' characteristics and their behavior or development
    - cannot infer cause and effect
*** Experimental Research Design
    - participants randomly assigned to treatment conditions
    - permits inferences about cause and effect
    - findings obtained in laboratories may not apply to everyday situations
*** Experimental Design
**** Independent Variable
     - manipulated by investigator
     - expected to cause changes in another variable
**** Dependent Variable
     - measured but not manipulated
     - expected to be influenced by the independent variable
*** Modified Experiments
**** Field Experiment
     - conducted in a natural setting
     - participants assigned randomly to treatment conditions
**** Natural, or Quasi-, Experiment
     - compare differences in treatments that already exist
     - groups chosen to ensure that their characteristics are as much alike as possible
*** Designs for Studying Development
**** Longitudinal
     - same participants studied repeatedly at different ages
***** Strengths
      - permits study of common patterns and individual differences
      - permits study of relationships between early and later events and behaviors
***** Limitations
      - participant dropout
      - practice effects
      - cohort effects
**** Cross-Sectional
     - participants of differing ages at the same point in time
***** Strengths
      - more efficient than longitudinal design
      - not plagued by problems like dropout and practice effects
***** Limitations
      - does not permit study of individual developmental trends
      - cohort effects
**** Sequential
     - several similar cross-sectional or longitudinal studies conducted at varying times
***** Strengths
      - permits longitudinal and cross-sectional comparisons
      - reveals cohort effects and tracks age-related changes efficiently
***** Limitations
      - may have same problems as longitudinal and cross-sectional strategies
      - but design itself helps identify difficulties
** Ethics in Lifespan Research
*** Rights of Research Participants
    - protection from harm
    - informed consent
    - privacy
    - knowledge of results
    - beneficial treatments
* Chapter 2 - Genetic and Environmental Foundations
** Introduction
*** Genotype
    - an individual's unique gentetic information
*** Phenotype
    - an individual's directly observable characteristics
** Genetic Foundations
*** Chromosomes
    - store and transmit genetic information
*** DNA
    - substance of which genes and chromosomes are made
*** Gene
    - a segment of DNA located along the chromosomes
*** Meiosis
    - process of cell division that creates gametes (sex cells)
    - halves the number of cell chromosomes
    - when sperm and ovum unite, zygote will have 46 chromosomes again
    - facilitates genetic variability that is adaptive
*** Autosomes, Sex Chromosomes, and Sex Cells
**** Autosomes
     - 22 matching pairs of chromosomes
**** Sex Chromosomes
     - 23rd pair of chromosomes
     - XX = female
     - XY = male
**** Gametes
     - sex cells
       - sperm and ovum
**** Zygote
     - formed when sperm and ovum unite
*** Twins
**** Fraternal/dizygotic Twins
     - two zygotes, or fertilized ova
**** Identical/monozygotic Twins
     - one zygote that divides into two individuals
*** Alleles
    - two forms of the same gene, one inherited from each parent
    - occur at the same place on both chromosomes in a pair
**** Homozygous
     - both alleles are alike
**** Heterozygous
     - alleles differ
*** Dominant-Recessive Inheritance
    - only the dominant allel affects child's characteristics
    - recessive allele can be passed to children
    - many serious disabilities and diseases result from recessive alleles
*** Incomplete Dominance
    - both alleles are expressed in the phenotype
    - results in a combined trait, or one that is intermediate between the two
    - e.g. sickle cell anemia
*** X-Linked Inheritance
    - a harmful allele is carried on the X chromosome
    - males more likely to be affected
      - Y chromosome does not have corresponding genes to override broken ones on X chromosome
    - e.g. hemophilia
*** Genomic Imprinting
    - chemical marker activates one allele in a pair
    - often temporary; may not pass to next generation
*** Mutation
    - sudden, permanent change in a DNA segment
    - can occur spontaneiously or be caused by hazardous environmental agents
    - types of mutations
      - germline
      - somatic
*** Polygenic Inheritance
    - many genes affect the characteristic in question
    - affects characteristics that vary among people, such as:
      - height
      - weight
      - intelligence
      - personality
*** Chromosomal Abnormalities
**** Down Symdrome
     - results when 21st chromosome pair fails to separate during meiosis
**** Sex Chromosome Abnormalities
     - caused by problems with X or Y chromosome
     - often not recognized until adolescence
** Reproductive Choices
*** Genetic Counseling
    - helps couples assess risk of hereditary disorders and choose best course of action
    - individuals likely to seek genetic counseling:
      - couple has had difficulties bearing children
      - known genetic problems exist
      - either parent is over 35
*** Prenatal Diagnostic Methods
    - amniocentesis
    - chorionic villus sampling
    - fetoscopy
    - ultrasound
    - maternal blood analysis
    - ultrafast magnetic resonance imaging
    - preimplantation genetic diagnosis
*** Fetal Medicine
    - drug administration to fetus
    - surgery
    - blood transfusions
    - bone marrow transplants
    - risks
      - complications
      - premature labor
      - miscarriage
*** Genetic Engineering
    - purpose is to correct hereditary defects
**** Human Genome Project
     - has identified thousands of genes
     - exploring new treatments, such as gene therapy and proteomics
*** Adoption
    - adoptees have more learning and emotional difficulties; possible causes:
      - genetic predispositions
      - stress of biological mother
      - history of conflict-ridden family relationships
      - adoptive parents and children less alike
    - with sensitive parenting, most adoptees fare well
** Environmental Contexts for Development
*** Family Influenes on Development
    - adapting to changes within and ouside family
      - e.g. birth of a baby
**** Direct
     - two-person relationships
       - e.g. parent, sibling, marital spouse
**** Indirect
     - third parties
       - e.g. healthy marriage fosters effective coparenting
*** Socioeconomic Status
    - index that combines three related variables:
      - years of education
      - prestige of one's job and skill it requires
      - income
    - first two measure social status; the last measures economic status
*** Socioeconomic Status and Family Functioning
    - SES is linked to:
      - timing of marriage and parenthood
      - family size
      - child-rearing values and expectations
      - communication and discipline styles
      - parents' education and economic security
      - children's cognitive and social development
*** Poverty
    - 21% of U.S. children live in poverty
      - rates higher for children of parents under age 25, ethnic minorities, and single mothers
    - risks of poverty
      - lifelong poor physical health
      - poor cognitive development and academic achievement
      - mental illness
      - antisocial behavior
      - hostile family interactions
*** Affluence
    - risks of affluence
      - poor academic achievement
      - alcohol and drug use
      - delinquency
      - anxiety and depression
    - unavailable parents
      - lack of emotional closeness and supervision
      - make excessive demands for achievement
*** Neighborhood
    - offers resources and social ties that promote development
    - greater impact on economically disadvantaged
    - in-school and after-school programs for low-income children show improved:
      - academic achievement
      - social adjustment
      - family functioning and child rearing
*** Schools
    - complex social systems that affect many aspects of development
    - achieving well in elementary and secondary school crucial to success in college
    - children in low-SES neighborhoods likely to experience poorer quality education
    - interventions to upgrade best begun early
*** Cultural Context
    - culture shapes all aspects of daily life
    - U.S. culture emphasizes independence, self-reliance, and family privacy
    - subcultures have beliefs and customs that differ from the larger culture
      - e.g. ethnic minority groups' cooperative family structures foster resilience and enhance child-rearing
*** Collectivism vs Individualism
**** Collectivism
     - stress group goals over individual goals
     - value interdependent qualities
       - e.g. responsibility to others, social harmony, collaborative endeavors
**** Individualism
     - concern with own personal needs
     - value independence
       - personal achievement, exploration, and choice in relationships
*** U.S. Public Policy Shortcomings
    - Advocacy groups and research help improve policy
**** Children and Youth
     - lack of affordable health insurance and quality child care
     - weak enforcement of child support payments
     - high school dropout rates
**** Older Adults
     - limited funding for social services
     - Social Security minimum is below poverty line
** Understanding the Relationship Between Heredity and Environment
*** Behavioral Genetics
    - explores contributions of nature and nurture to diversity of human traits and abilities
    - limited to investigating impact of heredity on complex characteristics indirectly
*** Heritability Estimates
    - measure extent to which individual differences in complex traits are due to heredity
    - obtained from kinship studies
      - e.g. comparisons of twins, or adoptees and their biological parents
    - estimates range from 0 to 1.0
      - 0.5 suggest heredity explains half the trait's variation
    - limitations
      - may not represet the population
      - can be misapplied
      - limited usefulness
*** Gene-Environment Interaction
    - individuals respond differently to same environment because of genetic makeup
    - similar responses can result from different gene-environment combinations
      - e.g. children with different environmental enrichment produce same intelligence test scores
*** Gene-Environment Correlation
    - our genes influence the environments to which we are exposed
**** Passive Correlation
**** Evocative Correlation
**** Active Correlation
     - niche-picking
*** Epigenesis
    - development resulting from ongoing, bidirectional exchanges
      - genes affect behavior and experiences
      - behavior and experiences affet gene expression
    - epigenetics studies how environment alters gene expression
      - e.g. methylation
** The Tutsi Genocide and Epigenetic Transmission of Maternal Stress to Children
   - exposure to extreme adversity increases methylation of a gene integral to stress-hormone regulation
   - Tutsi mothers pregnant during genocide and their children tested 18 years later
     - both had significantly lower stress-hormone levels
     - long-lasting effects evident in serious psychological disorders
   - prenatal exposure to severe maternal stress can induce peigenetic changes through methylation
* Chapter 3 - Prenatal Development, Birth, and the Newborn Baby
** Conception
   - sperm and ovum
   - ovum releases from ovaries once every 28 days; survives for one day
   - sperm live in fallopian tube for up to six days
   - most conceptions result from intercourse on day of ovulation or during two days preceding it
** Periods of Prenatal Development
   | Period              | Key Events                                        |
   |---------------------+---------------------------------------------------|
   | Germinal (2 weeks)  | fertilization and implantation; start of placenta |
   | Embryonic (6 weeks) | internal organs, all body structures              |
   | Fetal (30 weeks)    | growth and finishing phase                        |
*** Germinal Period
    - weeks 1-2
    - fertilization and formation of zygote
    - implantation of the blastocyst
    - development of feeding and protective structures
      - amnion
      - chorion
      - yolk sac
      - placenta
      - umbilical cord
*** Embryonic Period
    - weeks 3-8
    - most rapid prenatal changes occur
    - CNS, organs, muscles, skeleton begin to form
      - heart begins pumping blood
      - neurons form rapidly
*** Fetal Period
    - week 9 to the end of pregnancy
    - first trimester
      - organs, muscles, and nervous system
      - lungs expand and contract
      - sex of fetus visible
    - second trimester
      - active fetus
      - neurons rapidly form synapses
      - sensitivity to sound and light
    - third trimester
      - age of viability: 22-26 weeks
      - extensive body growth
      - rapid gains in neural connectivity and organizationg
      - greater responsiveness to external stimulation
** Prenatal Environmental Influences
*** Teratogens
    - environmental agents that cause damage during the prenatal period
    - efects depend on:
      - dose
      - heredity
      - presence of other negative factors
      - age and prenatal sensitive periods
    - delayed health effects may show up decades later
**** Teratogenic Substances
***** Common Teratogenic Drugs
      - prescription and non-prescription drugs
        - aspirin
        - caffeine
        - antidepressants
      - illegal drugs
        - cocaine, heroin, methadone
        - marijuana
***** Tobacco
      - 11% of U.S. women smoke while pregnant
      - low birth weight is best-known effect
      - increased risk of:
        - miscarriage
        - premature birth
        - abnormalities
        - disease
        - long-term learning, attention, and behavior problems
***** Fetal Alcohol Spectrum Disorder
      - range of outcomes caused by prenatal alcohol exposure
        - Fetal Alcohol Syndrome (FAS)
        - Partial Fetal Alcohol Syndrome (p-FAS)
        - Alcohol-related Neurodevelopmental Disorder (ARND)
      - consequences
        - slow physical growth
        - facial abnormalities
        - mental impairment
***** Radiation and Environmental Pollution
      - radiation
        - even low level exposure is harmful
        - e.g. x-rays, industrial leakage
      - environmental pollution
        - many babies "born polluted"
        - e.g. mercury, lead, dioxins, traffic-related pollution
      - can cause variety of physical defects and cognitive impairments, and increase chances of later illnesses
*** Infectious Disease
    - can cause extensive damage
    - threat greatest during embryonic period
    - infection during fetal period
*** Other Maternal Factors
    - nutrition
    - emotional stress
    - Rh factor incompatibility
    - maternal age
    - previous births
*** Nutrition
    - healthy pregnancy: 25- to 30-pound weight gain
    - prenatal malnutrition can:
      - damage organs and central nervous sysem
      - increase chances of later illness and disease
    - vitamin-mineral enrichment crucial
      - e.g. folic acid
*** Emotional Stress
    - deprives fetus of oxygen and nutrients
    - stress hormones cross the placenta, causing a dramatic rise in fetal stress hormones
    - associated with negative behavioral outcomes in children and youth and risk of later-life diseases
    - social support greatly reduces risk of complications
*** Importance of Prenatal Care
    - monitor general health, and growth of fetus
    - treat complications
    - lack of early care linked to greater likelihood of low birth weight and fetal death
    - public education and culturally sensitive practices needed
** Childbirth
*** Stages of Childbirth
    - dialation and effacement of cervix
    - delivery of baby
    - delivery of placenta
*** Baby's Adaptation to Labor and Delivery
    - strong contractions reduce baby's oxygen supply
    - infant cortisol and other stress hormones released:
      - help baby withstand oxygen deprivation
      - prepare baby to breath
      - arouse infant into alertness
*** Apgar Scale
    - quickly assesses the newborn's physical condition:
      - heart rate
      - respiratory effort
      - refles irritability
      - muscle tone
      - color
    - ratings scale (1-10):
      - 7 or better: good physical condition
      - 4-6: baby needs assistance
      - 1-3: serious danger, needs emergency medical attention
** Approaches to Childbirth
*** Natural, or Prepared, Childbirth
    - techniques to reduce pain and medical intervention:
      - classes
      - relaxation and breathing techniques
      - labor coach: freind, relative, or trained doula
    - social support/doula:
      - fewer instrument-assisted or cesarean deliveries
      - less need for pain medication
      - higher Apgar scores
*** Home Delivery
    - often handled by certified nurse-midwives
    - well-trained attendants necessary to recognize and handle emergencies
    - if at risk for complication, should deliver in hospital
** Medical Interventions
*** Birth Complications
    - anoxia
      - inadequate oxygen supply
    - breech positon
      - may compress umbilical cord
*** Fetal Monitoring
*** Labor and Delivery Medication
    - analgesics
    - anesthetics
*** Cesarean Delivery
    - once rare, now 33% of U.S. deliveries
** Preterm and Small-for-Date Infants
*** Preterm
    - born weeks before their due date
    - smaller weight may be appropriate for length of pregnancy
    - 1 to 2 more weeks in womb greatly reduces rates of illness
*** Small-for-date
    - below expected weight considering length of pregnancy
    - may be full-term or preterm
    - inadequate nutrition before birth
    - usually more serious problems, especially if also preterm
*** Interventions for Preterm Infants
    - temperature-controlled isolette
    - feeding tube, respirator
    - special stimulation:
      - gentle rocking
      - auditory stimulation
      - touch, kangaroo care
** A Cross-National Perspective on Health Care and Other Policies for Parents and Newborns
** Newborn Baby's Capacities
*** Newborn Reflexes
    - adaptive value
      - survival
      - evoking interaction from caregivers
      - motor development
    - tested to reveal health of baby's nervous system
*** Infant Stages of Arousal
    - rapid-eye-movement sleep
    - non-rapid-eye-movement sleep
    - drowsiness
    - quiet alertness
    - waking activity and crying
*** Sleep States
    - REM sleep
      - brain-wave activity similar to waking state
      - newborns spend more time in REM sleep than ever again
      - believed to support nervous system development
    - NREM sleep
      - body is almost motionless
      - heart rate, breathing, and brain-wave activity slow and even
    - can help identify central nervous system abnormalities
*** Infant Crying
    - usually due to physical needs
    - common soothing techniques
      - talking softly or rhythmic sounds
      - offering pacifier
      - swaddling
      - holding on shoulder and rocking or walking
*** Newborn Sense of Touch
    - well-developed at birth
    - stimulates early physical growth and is vital for emotional development
    - used to investigate their world
    - highly sensitive to pain; anesthetics, sugar solution, breast milk, and touch ease pain
*** Newborn Senses of Taste and Smell
    - preferences present at birth are key for survival
    - prefers sweet tastes and breast milk over formula
    - amniotic fluid from mother's diet influences newborn's preferences
    - can locate odors and identify mother by smell
*** Newborn Sense of Hearing
    - can hear wide variety of sound patterns
    - prefers complex sounds to pure tones
    - can distinguish sound patterns at only a few days old
    - sensitive to mother's voice and human speech
      - biologically prepared to learn language
*** Newborn Sense of Vision
    - least-developed sense at birth
    - unable to see long distances or focus clearly
    - scans environment and tracks moving objects
    - prefers bright over gray, but does not discriminate colors
    - color vision becomes adult-like after 4 months
** Adjusting to the New Family Unit
*** Hormonal Changes
    - prepare expectant parents:
      - mother: more oxytocin (contractions, milk production, and caregiving)
      - father: rise in prolactin and estrogens, drop in androgens
      - effects depend on experience
    - foster and adoptive mothers: holding and interacting with infant releases oxytocin
*** Challenges of Early Weeks
    - new roles
    - changes scheduled
    - sleep deprivation
    - stress
