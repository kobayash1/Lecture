* Immunity
** Definition
   - physiological process that provides protection from disease
** Suppressed Immune Response
   - inadequately functioning immune system
*** Primary Immunodeficiency
    - entire immune system is inadequate
    - missing some or all components necessary for complete immune response
*** Secondary Immunodeficiency
    - loss of immune functioning as a result of disease or treatment
    - e.g.: medications, cancer treatments, specific cancers like Hodgkin's disease
** Exaggerated Immune Response
   - hypersensitivity disorder
   - can be localized or affect all body systems
   - 4 types
*** Type I
    - IgE-mediated
    - "allergic"
    - e.g.: seasonal allergic rhinitis, anaphylaxis
*** Type II
    - tissue-specific
    - e.g.: autoimmune thrombocytopenic purpura
*** Type III
    - immune complex-mediated
    - e.g.: systemic lupus erythematosus, RA
*** Type IV
    - cell-mediated or delayed hypersensitivity
    - e.g.: contact sensitivity to poison ivy, transplant rejection
** Risk Factors
*** Suppressed Immune Response
**** Age
     - very young and elderly
**** Nonimmunized State
     - those not getting their vaccines to rubella, mumps, etc.
**** Environmental Factors
     - poor nutrition
     - pollutants that may depress immune response
**** Chronic Illness
     - chronic conditions that can lead to reductions in immune function
       - DM, COPD, malnutrition
**** Medical Treatments
     - graft, transplant
       - medications given to reduce immune response
**** Genetics
**** High-Risk Behaviors and Substance Abuse
**** Pregnancy
     - fetal cells may remain in mother's tissues
     - leads to microchimerism
       - mixing of cells of different origins
*** Exaggerated Immune Response
**** Gender, Race, and Ethnicity
     - some hypersensitivity disorders higher incidence in certain groups
**** Genetics
**** Environmental or Medication Exposure
** Assessment
*** History
    - current and past medical problems including treatments
    - especially presence of conditions associated with immune problems
    - ask about allergies with response to exposure
    - current medications and vaccination history
    - problems during pregnancy
    - environmental stressors
    - history of exposure to pathogens that cause immunosuppression (HIV, hepatitis B, etc.)
*** Examination Findings
**** Optimal Findings
     - vital signs wnl
     - soft, movable, and nontendter lymph nodes
**** Suppressed Findings
     - weight loss
     - fatigue and malaise
     - impaired wound healing
     - opportunistic infections
     - psych eval due to inflammation and infection in CNS that can lead to depression
     - seizure activity, changes in motor behavior
**** Exaggerated Findings
     - sneezing, watery eyes, nasal congestion
     - rash, swelling, shock syndrome
     - often findings are vague and less obvious
     - also often affect multiple organ systems
     - symptoms become more apparent when system function is impaired
       - e.g.: pericarditis, renal failure, inability to control movements
*** Diagnostic Tests
**** Primary Testing
     - lab tests, blood tests, etc.
**** Allergy Testing
**** Disease-Specific Testing
     - ELISA tests
       - HIV, herpes simplex, more
**** Tests of Organ Function
     - A1C
       - DM management
     - hepatic function tests
     - thyroid screening panels
** Clinical Management
*** Primary Prevention
    - vaccines
    - avoiding high-risk behaviors
    - proper diet and exercise
*** Secondary Prevention
    - screening
*** Collaborative Interventions
    - treating infection, anaphylaxis, and other symptoms
    - see table 23-2 in Giddens
* Inflammation
  - can occur in presence or absence of infection
  - always present with infection
  - physiological response to injury
  - characterized by redness, swelling, warmth, and/or pain
  - caused by various sources
    - mechanical trauma
    - thermal, electrical, chemical injury
    - radiation damage
    - biological assault
      - viral, bacterial, fungal infections
** Normal Physiological Process
*** Inflammatory Response Functions
**** Restitution of Normal Functioning Cells After Injury
**** Fibrous Repair When Cells Cannot Be Restored
*** Chemotaxis
    - chemicals released that attract WBCs to area of inflammation
    - released in 4 ways
**** Bacterial or Viral Exotoxins
**** Degenerative By-products of Inflammation
**** Products of Complement System Activation
**** Reactive Products of Plasma Clotting in Inflamed Area
*** Proinflammatory Hormones
    - regulate inflammatory response
    - increase blood flow to injured area
    - increase vascular membrane permeability
    - activate components of immune response
    - stimulate growth of connective tissue
    - cause fever
    - three major hormone groups
**** Prostaglandins
**** Cytokines
**** Histamines
*** Exudate
    - interstifial fluids built up as a result of inflammatory response
**** Serous
     - clear
**** Sanguineous
     - containing red blood cells
**** Purulent
     - containing WBCs and bacteria
** Acute Inflammation
   - can last minutes to days
   - majority of activty in first 12-24 hours after tissue injury
   - may be localized or systemic
** Chronic Inflammation
   - marcrophages
     - facilitate hemostasis
     - remove necrotic tissue and foreign pathogen material
     - allow for repair and healing
   - healing processes interrupted by reinjury or renewed inflammation
   - systemic manifestations
     - body aches and pains
     - frequent infections
     - diarrhea
     - dry eyes
     - shortness of breath
** Consequences
   - negative consequences possible from inflammation
   - overly severe response can result in additional tissue damage
     - e.g.: spinal cord injury
   - inadequate response can lead to infection or chronic inflammation and illness
** Risk Factors
*** Populations At Risk
    - age important in development of more severe inflammatory responses
      - young immature or elderly immune system -> severe inflammation
    - uninsured or underinsured
*** Individual Risk Factors
    - presence of
      - autoimmune diseases
      - allergies
    - exposure to pathogens
    - genetics
    - certain chronic disease processes
** Assessment
*** History
    - determine nature of inflammatory trigger
    - patient's physiological ability to respond
    - risk for an ineffective inflammatory response
    - ask about presence of symptoms commonly associated with inflammation
      - swelling, pain, fatigue
    - determine duration of inflammatory process and treatment measures already initiated
*** Examination Findings
    - obvious trauma or minor wounds
    - swelling
    - drainage or pus from laceration
    - infection
    - inflammation associated with an immune response
      - presents with some combination of
        - swelling
        - pain
        - fever
        - decreased or absent functioning of tissue or organ
    - severe inflammation associated with
      - shock
      - multiorgan failure
      - significant hyperthermia
      - seizure
      - coma
      - death
*** Diagnostic Tests
    - blood tests
      - WBC counts help determine between acute and chronic inflammation
      - CRP and ESR confirm presence of inflammation
    - radiographic and other testing
** Clinical Management
*** Primary Prevention
    - reducing risk for injury and infection
    - hand hygiene precautions
    - keeping wounds clean and covered
    - using designated safety equipment in physical activity
    - food and water safety standards
*** Secondary Prevention
    - there are no routine screening procedures for inflammation
    - prevention strategies critical
*** Collaborative Interventions
**** RICE
     - rest, ice, compression, elevation
     - used for sprain or strain injuries
**** Immobilization Devices
     - splints, slings
**** Pharmacologic Agents
     - goals of treatment include
       - reducing inflammation
       - managing fever
       - providing pain relief
***** Steroidal Agents
      - glucocorticoids
      - used to suppress the immune system, suppressing inflammatory response
      - e.g. prednisone
***** Nonsteroidal Anti-Inflammatory Drugs
      - NSAID
      - e.g. naproxen, ibuprofen
      - inhibits COX-1 and COX-2
        - these enzymes mediate inflammation
      - newer NSAIDs inhibit only COX-2
***** Recombinant DNA and Monoclonal Antibodies
***** Antipyretics
      - fever management
      - e.g. acetaminophen, aspirin
***** Analgesics
      - pain relief caused by inflammation
      - nonopioids
        - NSAIDs, aspirin, acetaminophen
      - opioids
        - for sever pain
***** Antimicrobials
* Infection
  - invasion and multiplication of microbes in body tissues
  - acute or chronic
  - localized
    - limited to specific body area
  - disseminated
    - spread of infection from initial site to other areas of body
  - systemic
    - infection affecting whole body
** Infectious Process
*** Incubation
    - entrance of pathogen to appearance of first symptoms
    - 1 to several days
*** Prodromal
    - from onset of nonspecific signs and symptoms to more specific symptoms
    - microbe gorws and multiplies, host capable of spreading disease
*** Illness
    - time when host presents symptoms specific to infection
    - e.g. mumps, associated with high fever, parotid and salivary gland swelling
*** Convalescence
    - acute symptoms of infection disappear
    - recover can take days to months
** Normal Physiological Process
*** Infection Process
    - 6 elements
      - pathogen
      - host
      - reservoir
      - portal of entry
      - portal of exit
      - mode of transmission

** Consequences
   - severe infections can lead to shock, organ failure
     - symptoms may include
       - hypotension
       - tachycardia
       - tachypnea
       - oliguria
       - anuria
       - hypoxia
       - hypercapnia
       - seizures
       - coma
** Vascular, Renal, Nervous System Compensation
   - increased vascular permeability will lead to hypovolemia and hypotension
   - nervous system compensates for hypotension
     - peripheral vascular constriction
     - shunting blood from nonessential organs to essential organs
   - acute systemic hypoperfusion occurs
     - leads to increased heart rate in effort to maintain cardiac output
   - renal system compensates by creating vasodilatory response of glomeruli
   - continued hypoperfusion leads to decreased urine output to retain volume
     - leads to oliguria or anuria
   - extra stress on the heart due to peripheral vasoconstriction
** Respiratory Compensation
   - inadequate tissue perfusion (oxygenation)
   - respiratory system compensates by increasing rate of respiration
   - process can be hampered by cardiovascular decompensation
     - leads to decreased cardiac output, fluid accumulation, and pulmonary edema
   - inadequate tissue perfusion (hypoxia) leads to CNS decompensation
     - evidenced by early change in mental status progressing to seizures, stupor, coma
** Multisystem Failure
   - what eventually happens if infection left untreated
** Risk Factors
*** Populations At Risk
    - risk factors based on
      - age
      - socioeconomic status
      - geographic location
    - uninsured, underinsured
*** Individual Risk Factors
**** Compromised Host Due To Immunodeficiency
     - young
     - elderly
     - genetics
     - malnutrition
     - acute psychological stress
     - medications that induce immunosuppression
**** Compromised Host Due To Chronic Disease
     - COPD
     - influenza
     - pneumonia
     - DMII
     - hepatic and respiratory disorders
     - treatment strategies
       - invasive lines
       - corticosterioids
       - antibiotics
       - surgery
**** Envirionmental Conditions
     - crowded living conditions
     - access to clean food and water
** Assessment
*** History
    - ask questions to assess risk for infection
      - incidence of injury
      - known exposure to pathogens
      - history of prior infections
      - current treatment for cancer
      - recent surgical procedures
      - presence of autoimmune or immunodeficiency disease
      - use of immunosupporessant medication
      - travel
    - recognize symptoms associated with infection
      - symptoms verbalized by patient
        - often caused by inflammatory response to infection
          - e.g. pain, swelling, redness
      - specific symptoms influenced by affected organ
        - e.g. cough due to infection of respiratory system
      - some symptoms result of generalized stress to the host
        - e.g. fatigue or malaise
    - consider factors associated with presenting infection
*** Examination Findings
    - physical examination
    - presence of infection assessed by looking for
      - fever
      - swelling
      - chills
      - malaise
      - redness
      - wound drainage
      - pain
      - respiratory congestion
*** Diagnostic Tests
**** Laboratory Tests
***** Complete Blood Count
      - CBC with WBC differential count
      - evaluate presence of infection
      - elevated WBC indicate infection
***** Culture and Sensitivity
      - identify invading pathogen
      - specimen obtained from body
**** Radiographic Tests
     - determine extent and scope of confirmed infections
     - identify area of infection and inflammation
** Clinical Management
*** Primary Prevention
    - infection control measures
**** Hand Hygiene and Standard Precautions
**** Immunizations
*** Secondary Prevention
    - screening
    - less effective than prevention
    - opportunity to identify infection for earlier treatment and reducing transmission
    - screening for STDs
*** Collaborative Interventions
    - treatment of infectious process and support for affected body systems
**** Antimicrobials
     - treat various infections with antibiotics, antivirals
     - instruct to complete full course of antimicrobial therapy
       - avoids secondary infection or re-emergence of infection
**** Nutrition and Fluids
     - restore fluids and electrolytes
     - adequate rest and nutrition for support of immune system
* Acetaminophen
  - nonopioid, not an NSAID
  - no anti-inflammatory properties
  - does not interfere with platelet aggregation
  - treats pain and fever
  - no link to Reye syndrome
  - does not increase potential for excessive bleeding if taken for dysmenorrhea
** Pharmacokinetics
   - well absorbed in GI tract
   - erratic absorption in rectal canal
   - administer Q4h PRN
   - max dose of 4 g/day
   - suggested 2 g/day max dose for frequent users
     - avoid possibility of hepatic or renal dysfunction
   - >85% metabolized to drug metabolites by the liver
   - large doses can be toxic to hepatic cells
** Pharmacodynamics
   - acetaminophen weakly inhibits prostaglandin synthesis
     - decreases pain sensation
   - treat pain and headache
   - onset within 10 to 30 minutes when administered orally
   - 3 to 5 hour duration of action
** Side Effects and Adverse Reactions
   - overdose can be extremely toxic to liver cells
   - death can occur in 1 to 4 days from hepatic necrosis
   - early symptoms of hepatic damage include
     - nausea
     - vomiting
     - diarrhea
     - abdominal pain
** Nursing Process
*** Assessment
    - history of liver dysfunction?
    - severity of patient's pain?
*** Nursing Diagnoses
    - injury, risk for
    - pain, acute related to edema from surgical incision
*** Planning
    - the patient's pain will be relieved or diminished
*** Nursing Interventions
    - check hepatic enzyme tests for patients with high or overdoses
**** Patient Teaching
***** General
      - teach to keep acetaminophen away from children
      - advise patients not to self-medicate for more than 10 days
      - direct parents to call poison control center in event of child overdose
      - teach to avoid alcohol ingestion while taking acetaminophen
***** Side Effects
      - encourage patients to report side effects
* Antiinflammatories
** Pathophysiology
*** 5 Cardinal Signs of Inflammation
**** Pain
**** Swelling
**** Redness
**** Heat
**** Loss of Function
*** 2 Phases of Inflammation
**** Vascular Phase
     - occurs 10 to 15 minutes after injury
     - vasodilation and increased capillary permeability
**** Delayed Phase
     - occurs when leukocytes enter inflamed tissue
*** Chemical Mediators
**** Prostaglandins
     - vasodilation
     - relaxation of smooth muscle
     - increased capillary permeability
     - sensitization of nerve cells to pain
**** Cyclooxygenase (COX)
     - convert arachidonic acid into prostaglandins
     - causes inflammation and pain at tissue injury site
***** COX-1
      - protects stomach lining
      - regulates blood platelets
***** COX-2
      - triggers inflammation and pain
** Antiinflammatory Agents
   - drugs that inhibit synthesis of prostaglandin
   - prostaglandin inhibitors
     - commonly called antiinflammatory drugs
   - relieve pain
   - reduce elevated body temperature
   - inhibit platelet aggregation
** Nonsteroidal Antiinflammatory Drugs
   - NSAID
   - inhibit COX
   - prostaglandin inhibitors
   - used primarily for inflammation and pain
   - OTC's
     - aspirin
     - ibuprofen
     - naproxen
   - 7 groups of NSAIDs
*** Salicylates
    - aspirin
      - also called acetylsalicylic acid (ASA)
      - antiinflammatory and anti-platelet aggregation
      - gastric distress common problem side effect
        - enteric coated aspirin commonly used
      - should not be taken with other NSAID's
      - blocks both COX-1 and 2
**** Pharmacokinetics
     - aspirin is well absorbed in GI tract
     - can cause GI distress
     - EC (enteric coated) can help reduce GI distress
       - should not be crushed
     - short half-life
     - avoid use in children
       - can lead to fatal Reye syndrome
**** Pharmacodynamics
     - patients may be hypersensitive to aspirin
       - tinnitus
       - vertigo
       - bronchospasm
*** Propionic Acid Derivatives
    - stronger effects than aspirin, but less GI irritaion
    - ibuprofen
**** Pharmacokinetics
     - ibuprofens well absorbed in GI tract
     - short half-life
     - highly protein bound
       - severe side effects when taken with other highly-protein bound drug
     - metabolized in liver
     - excreted in urine
**** Pharmacodynamics
     - ibuprofen inhibits prostaglandin synthesis
     - effective in reducing pain and inflammation
     - many drug interactions
       - warfarin
       - sulfonamides
       - cephalosporins
       - phenytoin
       - aspirin
       - hypoglycemic drugs
       - calcium channel blockers
*** Para-Chlorobenzoic Acid
*** Phenylacetic Acid Derivatives
*** Fenamates
*** Oxicams
*** Selective COX-2 Inhibitors
    - block COX-2 but not COX-1
    - celecoxib
** Corticosteroids
   - prednisone, prednisolone, dexamethasone
   - antiinflammatory agents
   - suppress or prevent many components of inflammatory process
   - commonly used for arthritic flareups
   - long half-life (over 24 hours)
   - large doses prescribed
   - discontinuation of treatment requires taper over 5 to 10 days
* Glucocorticoids
