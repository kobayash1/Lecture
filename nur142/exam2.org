* Health Promotion for the Adolescent
** Adolescent Growth and Development
   - age 11-21
   - identity formation
*** Physical Growth and Development
    - secondary sex characteristics
*** Psychosexual Development, Hormonal Changes, Sexual Mauration
    - production of sex hormones, maturation of reproductive system
    - females reach maturation sooner than males
    - menarche
      - establishment of menstruation
      - occurs between 9 and 15
      - racial and ethnic differences
        - Hispanics and blacks before whites
    - females reach reproductive maturity 2-5 years after menarche
    - obesity can bring onset of puberty early
    - sexual maturity rating
      - SMR
      - Tanner's 5 stages
*** Motor Development
    - sports, exercise, etc.
    - bones not fully calcified
      - more resilient
*** Cognitive Development
